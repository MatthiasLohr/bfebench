#!/bin/sh
kubectl -n bfebench get pods | grep Completed | awk '{print $1}' | xargs kubectl -n bfebench delete pod
