#!/usr/bin/env python3

import argparse
import os
import re
import sys
from glob import glob
from statistics import mean, stdev
from typing import List, NamedTuple

from tabulate import tabulate

GAS_USED_PATTERN = re.compile(r"(.*[^0-9])(?P<gas>[0-9]+)( gas used.*)")

EMPTY_PLACEHOLDER = "-"

FILE_SIZES = ["1KiB", "2KiB", "4KiB", "8KiB", "16KiB", "32KiB", "64KiB", "128KiB", "256KiB", "512KiB", "1MiB"]

FAIRSWAP_STATIC_ROWS = [
    ("Contract deployment", "deployed contract", None),
    (
        "Accept",
        None,
        ".accept()",
    ),
    (
        "Key Revelation",
        ".revealKey()",
        None,
    ),
    (
        "Refund",
        "Seller invoked FileSale.refund()",
        "Buyer invoked FileSale.refund()",
    ),
]

FAIRSCE_STATIC_ROWS = [
    (
        "Adjudicator contract deployment",
        "deployed adjudicator contract",
        None,
        None,
    ),
    (
        "Asset Holder contract deployment",
        "deployed asset holder contract",
        None,
        None,
    ),
    (
        "App contract deployment",
        "deployed app contract",
        None,
        None,
    ),
    (
        "AssetHolder Deposit",
        None,
        None,
        "Buyer invoked AssetHolderETH.deposit()",
    ),
    ("Register Dispute", None, "Seller invoked Adjudicator.register()", "Buyer invoked Adjudicator.register()"),
    ("Progress Dispute", None, "Seller invoked Adjudicator.progress()", "Buyer invoked Adjudicator.progress()"),
    ("Conclude Dispute", None, "Seller invoked Adjudicator.conclude()", "Buyer invoked Adjudicator.conclude()"),
    (
        "Conclude with Consent",
        None,
        "Seller invoked Adjudicator.concludeFinal()",
        None,
    ),
    (
        "Asset Holder withdraw",
        None,
        "Seller invoked AssetHolderETH.withdraw()",
        "Buyer invoked AssetHolderETH.withdraw()",
    ),
    (
        "Extra Deposit contract deployment",
        None,
        "deployed extra deposit contract",
        None,
    ),
    (
        "Extra Deposit deposit",
        None,
        "Seller sent a direct transaction to FileSaleMonoDeposit",
        None,
    ),
    (
        "Extra Deposit init state channel",
        None,
        None,
        "Buyer invoked FileSaleMonoDeposit.doStateChannelDeposits()",
    ),
    (
        "Extra Deposit withdraw",
        None,
        "Seller invoked FileSaleMonoDeposit.withdraw()",
        None,
    ),
]


class CostResult(NamedTuple):
    mean: int
    min: int
    max: int
    stdev: int
    len: int


def log_lines_glob(glob_str: str) -> List[str]:
    log_files = glob(glob_str)
    log_lines = []
    for log_file in log_files:
        with open(log_file) as fp:
            log_lines.extend([line.strip() for line in fp.readlines()])
    return log_lines


def main() -> int:
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("log_directory", help="Directory with bfebench log files")
    argument_parser.add_argument("--show-fairswap-static", action="store_true")
    argument_parser.add_argument("--show-fairswap-dynamic", action="store_true")
    argument_parser.add_argument("--show-fairsce-static", action="store_true")
    argument_parser.add_argument("--show-fairsce-dynamic", action="store_true")
    args = argument_parser.parse_args()

    if args.show_fairswap_static:
        log_lines = log_lines_glob(os.path.join(args.log_directory, "bfebench-Fairswap-*.log"))

        result_lines = []
        for fairswap_row_definition in FAIRSWAP_STATIC_ROWS:
            seller_cr = (
                None if fairswap_row_definition[1] is None else get_cost_result(log_lines, fairswap_row_definition[1])
            )
            buyer_cr = (
                None if fairswap_row_definition[2] is None else get_cost_result(log_lines, fairswap_row_definition[2])
            )

            result_lines.append(
                [
                    fairswap_row_definition[0],
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.mean),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.stdev),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.min),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.max),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.len),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.mean),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.stdev),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.min),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.max),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.len),
                ]
            )

        print(tabulate(result_lines, tablefmt="latex_raw"))

    if args.show_fairswap_dynamic:
        result_lines = []
        for file_size in FILE_SIZES:
            complain_about_root_cr = get_cost_result(
                log_lines_glob(os.path.join(args.log_directory, "bfebench-Fairswap*-%s.log" % file_size)),
                ".complainAboutRoot()",
            )
            complain_about_node_cr = get_cost_result(
                log_lines_glob(os.path.join(args.log_directory, "bfebench-Fairswap*-%s.log" % file_size)),
                ".complainAboutNode()",
            )
            complain_about_leaf_cr = get_cost_result(
                log_lines_glob(os.path.join(args.log_directory, "bfebench-Fairswap*-%s.log" % file_size)),
                ".complainAboutLeaf()",
            )

            result_lines.append(
                [
                    file_size,
                    "{:,}".format(complain_about_root_cr.mean),
                    "{:,}".format(complain_about_root_cr.stdev),
                    "{:,}".format(complain_about_root_cr.min),
                    "{:,}".format(complain_about_root_cr.max),
                    "{:,}".format(complain_about_root_cr.len),
                    "{:,}".format(complain_about_node_cr.mean),
                    "{:,}".format(complain_about_node_cr.stdev),
                    "{:,}".format(complain_about_node_cr.min),
                    "{:,}".format(complain_about_node_cr.max),
                    "{:,}".format(complain_about_node_cr.len),
                    "{:,}".format(complain_about_leaf_cr.mean),
                    "{:,}".format(complain_about_leaf_cr.stdev),
                    "{:,}".format(complain_about_leaf_cr.min),
                    "{:,}".format(complain_about_leaf_cr.max),
                    "{:,}".format(complain_about_leaf_cr.len),
                ]
            )

        print(tabulate(result_lines, tablefmt="latex_raw"))

    if args.show_fairsce_static:
        log_lines = log_lines_glob(os.path.join(args.log_directory, "bfebench-StateChannelFileSale*.log"))

        result_lines = []
        for fairsce_row_definition in FAIRSCE_STATIC_ROWS:
            operator_cr = (
                None if fairsce_row_definition[1] is None else get_cost_result(log_lines, fairsce_row_definition[1])
            )
            seller_cr = (
                None if fairsce_row_definition[2] is None else get_cost_result(log_lines, fairsce_row_definition[2])
            )
            buyer_cr = (
                None if fairsce_row_definition[3] is None else get_cost_result(log_lines, fairsce_row_definition[3])
            )

            result_lines.append(
                [
                    fairsce_row_definition[0],
                    EMPTY_PLACEHOLDER if operator_cr is None else "{:,}".format(operator_cr.mean),
                    EMPTY_PLACEHOLDER if operator_cr is None else "{:,}".format(operator_cr.stdev),
                    EMPTY_PLACEHOLDER if operator_cr is None else "{:,}".format(operator_cr.min),
                    EMPTY_PLACEHOLDER if operator_cr is None else "{:,}".format(operator_cr.max),
                    EMPTY_PLACEHOLDER if operator_cr is None else "{:,}".format(operator_cr.len),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.mean),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.stdev),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.min),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.max),
                    EMPTY_PLACEHOLDER if seller_cr is None else "{:,}".format(seller_cr.len),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.mean),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.stdev),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.min),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.max),
                    EMPTY_PLACEHOLDER if buyer_cr is None else "{:,}".format(buyer_cr.len),
                ]
            )

        print(tabulate(result_lines, tablefmt="latex_raw"))

    if args.show_fairsce_dynamic:
        result_lines = []
        for file_size in FILE_SIZES:
            complain_about_root_cr = get_cost_result(
                log_lines_glob(os.path.join(args.log_directory, "bfebench-StateChannelFileSale*-%s.log" % file_size)),
                "Buyer invoked FileSaleApp.complainAboutRoot()",
            )
            complain_about_node_cr = get_cost_result(
                log_lines_glob(os.path.join(args.log_directory, "bfebench-StateChannelFileSale*-%s.log" % file_size)),
                "Buyer invoked FileSaleApp.complainAboutNode()",
            )
            complain_about_leaf_cr = get_cost_result(
                log_lines_glob(os.path.join(args.log_directory, "bfebench-StateChannelFileSale*-%s.log" % file_size)),
                "Buyer invoked FileSaleApp.complainAboutLeaf()",
            )

            result_lines.append(
                [
                    file_size,
                    "{:,}".format(complain_about_root_cr.mean),
                    "{:,}".format(complain_about_root_cr.stdev),
                    "{:,}".format(complain_about_root_cr.min),
                    "{:,}".format(complain_about_root_cr.max),
                    "{:,}".format(complain_about_root_cr.len),
                    "{:,}".format(complain_about_node_cr.mean),
                    "{:,}".format(complain_about_node_cr.stdev),
                    "{:,}".format(complain_about_node_cr.min),
                    "{:,}".format(complain_about_node_cr.max),
                    "{:,}".format(complain_about_node_cr.len),
                    "{:,}".format(complain_about_leaf_cr.mean),
                    "{:,}".format(complain_about_leaf_cr.stdev),
                    "{:,}".format(complain_about_leaf_cr.min),
                    "{:,}".format(complain_about_leaf_cr.max),
                    "{:,}".format(complain_about_leaf_cr.len),
                ]
            )

        print(tabulate(result_lines, tablefmt="latex_raw"))

    return 0


def get_cost_result(lines: List[str], selector_pattern_str: str, is_regexp: bool = False) -> CostResult:
    if not is_regexp:
        selector_pattern_str = re.escape(selector_pattern_str)

    selector_pattern = re.compile(r"(.*)" + selector_pattern_str + "(.*)")
    selected_lines = filter(selector_pattern.match, lines)
    gas = [int(GAS_USED_PATTERN.match(line).group("gas")) for line in selected_lines]  # type: ignore

    return CostResult(mean=round(mean(gas)), min=min(gas), max=max(gas), stdev=round(stdev(gas)), len=len(gas))


if __name__ == "__main__":
    sys.exit(main())
