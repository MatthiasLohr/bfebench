# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from bfebench.protocols.fairswap.strategies import FaithfulSeller
from bfebench.protocols.fairswap.util import (
    encode,
    encode_forge_first_leaf,
    encode_forge_first_leaf_first_hash,
)
from bfebench.utils.bytes import generate_bytes
from bfebench.utils.merkle import MerkleTreeNode


class NodeForgingSeller(FaithfulSeller):
    def encode_file(self, data_merkle: MerkleTreeNode, data_key: bytes) -> MerkleTreeNode:
        return encode_forge_first_leaf_first_hash(data_merkle, data_key)


class LeafForgingSeller(FaithfulSeller):
    def encode_file(self, data_merkle: MerkleTreeNode, data_key: bytes) -> MerkleTreeNode:
        return encode_forge_first_leaf(data_merkle, data_key)


class RootForgingSeller(FaithfulSeller):
    def encode_file(self, data_merkle: MerkleTreeNode, data_key: bytes) -> MerkleTreeNode:
        return encode(data_merkle, generate_bytes(32, avoid=data_key))
