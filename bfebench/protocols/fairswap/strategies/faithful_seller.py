# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from math import log2

from eth_typing.evm import ChecksumAddress
from web3.types import Wei

from bfebench.contract import SolidityContract, SolidityContractSourceCodeManager
from bfebench.environment import Environment, EnvironmentWaitResult
from bfebench.protocols.fairswap import Fairswap
from bfebench.protocols.fairswap.strategies.signals import ExitSignal
from bfebench.protocols.fairswap.util import encode, keccak
from bfebench.utils.bytes import generate_bytes
from bfebench.utils.json_stream import JsonObjectSocketStream
from bfebench.utils.merkle import MerkleTreeNode, from_bytes, mt2obj

from ....protocols.strategy import SellerStrategy


class FaithfulSeller(SellerStrategy[Fairswap]):
    def run(
        self,
        environment: Environment,
        p2p_stream: JsonObjectSocketStream,
        opposite_address: ChecksumAddress,
    ) -> None:
        # === PHASE 1: transfer file / initialize (deploy contract) ===
        # transmit encrypted data
        with open(self.protocol.filename, "rb") as fp:
            data = fp.read()
        data_merkle = from_bytes(data, keccak, slice_count=self.protocol.slice_count)
        data_key = generate_bytes(32)
        data_merkle_encrypted = self.encode_file(data_merkle, data_key)

        # deploy contract
        scscm = SolidityContractSourceCodeManager()
        scscm.add_contract_template_file(
            os.path.join(os.path.dirname(__file__), Fairswap.CONTRACT_TEMPLATE_FILE),
            {
                "merkle_tree_depth": log2(self.protocol.slice_count) + 1,
                "slice_length": self.protocol.slice_length,
                "slice_count": self.protocol.slice_count,
                "receiver": str(opposite_address),
                "price": self.protocol.price,
                "key_commitment": "0x" + keccak(data_key).hex(),
                "ciphertext_root_hash": "0x" + data_merkle_encrypted.digest.hex(),
                "file_root_hash": "0x" + data_merkle.digest.hex(),
                "timeout": self.protocol.timeout,
            },
        )
        contracts = scscm.compile(Fairswap.CONTRACT_SOLC_VERSION)
        contract = contracts[Fairswap.CONTRACT_NAME]
        tx_receipt = environment.deploy_contract(contract)
        self.logger.debug("deployed contract at %s (%s gas used)" % (contract.address, tx_receipt["gasUsed"]))
        web3_contract = environment.get_web3_contract(contract)

        p2p_stream.send_object(
            {
                "contract_address": contract.address,
                "contract_abi": contract.abi,
                "tree": mt2obj(data_merkle_encrypted, encode_func=lambda b: bytes(b).hex()),
            }
        )

        # === PHASE 2: wait for buyer accept ===
        self.logger.debug("waiting for accept")
        result = environment.wait(
            timeout=web3_contract.functions.timeout().call() + 1,
            condition=lambda: web3_contract.functions.phase().call() == 2,
        )
        if result == EnvironmentWaitResult.TIMEOUT:
            self.logger.debug("timeout reached, requesting refund")
            environment.send_contract_transaction(contract, "refund")
            return
        self.logger.debug("accepted")

        # === PHASE 3: reveal key ===
        try:
            self.reveal_key(environment, contract, data_key)
        except ExitSignal:
            return

        # === PHASE 5: finalize
        self.logger.debug("waiting for confirmation or timeout...")
        result = environment.wait(
            timeout=web3_contract.functions.timeout().call() + 1,
            condition=lambda: not environment.web3.eth.get_code(contract.address),
        )
        if result == EnvironmentWaitResult.TIMEOUT:
            self.logger.debug("timeout reached, requesting refund")
            environment.send_contract_transaction(contract, "refund")
            return

    def encode_file(self, data_merkle: MerkleTreeNode, data_key: bytes) -> MerkleTreeNode:
        return encode(data_merkle, data_key)

    def reveal_key(self, environment: Environment, contract: SolidityContract, data_key: bytes) -> None:
        environment.send_contract_transaction(contract, "revealKey", data_key, gas_limit=Wei(65000))
