# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from eth_typing.evm import ChecksumAddress
from web3.types import Wei

from bfebench.contract import Contract
from bfebench.environment import Environment, EnvironmentWaitResult
from bfebench.protocols.fairswap.util import (
    B032,
    LeafDigestMismatchError,
    NodeDigestMismatchError,
    crypt,
    decode,
    keccak,
)
from bfebench.utils.json_stream import JsonObjectSocketStream
from bfebench.utils.merkle import obj2mt

from .buyer import FairswapBuyer


class FaithfulBuyer(FairswapBuyer):
    def run(
        self,
        environment: Environment,
        p2p_stream: JsonObjectSocketStream,
        opposite_address: ChecksumAddress,
    ) -> None:
        # === PHASE 1: wait for seller initialization ===
        init_info, byte_count = p2p_stream.receive_object()
        data_merkle_encrypted = obj2mt(
            data=init_info.get("tree"),
            digest_func=keccak,
            decode_func=lambda s: bytes.fromhex(str(s)),
        )
        contract = Contract(
            abi=init_info.get("contract_abi"),
            address=init_info.get("contract_address"),
            name=self.protocol.CONTRACT_NAME,
        )
        web3_contract = environment.get_web3_contract(contract)

        # === PHASE 2: accept ===
        if web3_contract.functions.fileRoot().call() == self.expected_plain_digest:
            self.logger.debug("confirming plain file hash")
        else:
            self.logger.debug("wrong plain file hash")
            return

        if web3_contract.functions.ciphertextRoot().call() == data_merkle_encrypted.digest:
            self.logger.debug("confirming ciphertext hash")
        else:
            self.logger.debug("wrong ciphertext hash")
            return

        tx_receipt = environment.send_contract_transaction(
            contract, "accept", value=self.protocol.price, gas_limit=Wei(50000)
        )
        self.logger.debug("Sent 'accept' transaction (%s Gas used)" % tx_receipt["gasUsed"])

        # === PHASE 3: wait for key revelation ===
        self.logger.debug("waiting for key revelation")
        result = environment.wait(
            timeout=web3_contract.functions.timeout().call() + 1,
            condition=lambda: web3_contract.functions.key().call() != B032,
        )
        if result == EnvironmentWaitResult.TIMEOUT:
            self.logger.debug("timeout reached, requesting refund")
            environment.send_contract_transaction(contract, "refund")
            return

        data_key = web3_contract.functions.key().call()
        self.logger.debug("key revealed")

        # === PHASE 4: complain ===
        if (
            crypt(data_merkle_encrypted.leaves[-2].data, 2 * self.protocol.slice_count - 2, data_key)
            != self.expected_plain_digest
        ):
            environment.send_contract_transaction(
                contract,
                "complainAboutRoot",
                data_merkle_encrypted.leaves[-2].digest,
                data_merkle_encrypted.get_proof(data_merkle_encrypted.leaves[-2]),
            )
            return
        else:
            data_merkle, errors = decode(data_merkle_encrypted, data_key)
            if len(errors) == 0:
                if self.protocol.send_buyer_confirmation:
                    environment.send_contract_transaction(contract, "noComplain")
                else:
                    self.logger.debug("file successfully decrypted, quitting.")
                    # not calling `noComplain` here, no benefit for buyer (rational party)
                return
            elif isinstance(errors[-1], LeafDigestMismatchError):
                error: NodeDigestMismatchError = errors[-1]
                environment.send_contract_transaction(
                    contract,
                    "complainAboutLeaf",
                    error.index_out,
                    error.index_in,
                    error.out.digest,
                    error.in1.data_as_list(),
                    error.in2.data_as_list(),
                    data_merkle_encrypted.get_proof(error.out),
                    data_merkle_encrypted.get_proof(error.in1),
                )
                return
            else:
                error = errors[-1]
                environment.send_contract_transaction(
                    contract,
                    "complainAboutNode",
                    error.index_out,
                    error.index_in,
                    error.out.digest,
                    error.in1.digest,
                    error.in2.digest,
                    data_merkle_encrypted.get_proof(error.out),
                    data_merkle_encrypted.get_proof(error.in1),
                )
                return
