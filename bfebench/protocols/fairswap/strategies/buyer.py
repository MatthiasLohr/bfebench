# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from eth_typing.evm import ChecksumAddress

from bfebench.environment import Environment
from bfebench.protocols.fairswap import Fairswap
from bfebench.protocols.fairswap.util import keccak
from bfebench.utils.json_stream import JsonObjectSocketStream
from bfebench.utils.merkle import from_bytes

from ...strategy import BuyerStrategy


class FairswapBuyer(BuyerStrategy[Fairswap]):
    def __init__(self, protocol: Fairswap) -> None:
        super().__init__(protocol)

        # caching expected file digest here to avoid hashing to be counted during execution
        with open(self.protocol.filename, "rb") as fp:
            data = fp.read()
        data_merkle = from_bytes(data, keccak, slice_count=self.protocol.slice_count)
        self._expected_plain_digest = data_merkle.digest

    @property
    def expected_plain_digest(self) -> bytes:
        return self._expected_plain_digest

    def run(
        self,
        environment: Environment,
        p2p_stream: JsonObjectSocketStream,
        opposite_address: ChecksumAddress,
    ) -> None:
        raise NotImplementedError()
