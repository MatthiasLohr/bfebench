# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from time import time
from typing import cast

from eth_typing.evm import ChecksumAddress

from bfebench.environment import Environment, EnvironmentWaitResult
from bfebench.protocols.state_channel_file_sale.strategies.buyer import (
    StateChannelFileSaleBuyer,
)
from bfebench.protocols.state_channel_file_sale_extra_deposit.protocol import (
    StateChannelFileSaleExtraDeposit,
)
from bfebench.utils.json_stream import JsonObjectSocketStream


class StateChannelFileSaleExtraMonoDepositBuyer(StateChannelFileSaleBuyer):
    def run(
        self,
        environment: Environment,
        p2p_stream: JsonObjectSocketStream,
        opposite_address: ChecksumAddress,
    ) -> None:
        self.logger.debug("Waiting for extra seller deposit...")
        result = environment.wait(
            timeout=time() + self.protocol.timeout,
            condition=lambda: environment.web3.eth.get_balance(self.protocol.extra_deposit_contract.address) > 0,
        )
        if result == EnvironmentWaitResult.TIMEOUT:
            self.logger.debug("timeout reached, no extra deposit from seller, quitting")
            return

        super().run(environment, p2p_stream, opposite_address)

    def fund_state_channel(self, environment: Environment, channel_id: bytes, funding_id: bytes) -> None:
        environment.send_contract_transaction(
            self.protocol.extra_deposit_contract,
            "doStateChannelDeposits",
            channel_id,
            environment.wallet_address,
            value=self.protocol.buyer_deposit,
        )

    @property
    def protocol(self) -> StateChannelFileSaleExtraDeposit:
        return cast(StateChannelFileSaleExtraDeposit, self._protocol)
