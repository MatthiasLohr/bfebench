# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import cast

from eth_typing.evm import ChecksumAddress

from bfebench.environment import Environment
from bfebench.protocols.state_channel_file_sale.strategies.seller import (
    StateChannelFileSaleSeller,
)
from bfebench.protocols.state_channel_file_sale_extra_deposit.protocol import (
    StateChannelFileSaleExtraDeposit,
)
from bfebench.utils.json_stream import JsonObjectSocketStream


class StateChannelFileSaleExtraMonoDepositSeller(StateChannelFileSaleSeller):
    def run(
        self,
        environment: Environment,
        p2p_stream: JsonObjectSocketStream,
        opposite_address: ChecksumAddress,
    ) -> None:
        # do extra deposit
        environment.send_direct_transaction(self.protocol.extra_deposit_contract, self.protocol.seller_deposit)

        # run file sale protocol
        try:
            super().run(environment, p2p_stream, opposite_address)
        except TimeoutError:
            pass

        # withdraw remaining extra funds
        remaining_deposit_amount = environment.web3.eth.get_balance(self.protocol.extra_deposit_contract.address)
        if remaining_deposit_amount > 0:
            self.logger.debug("Remaining extra deposit of %d, withdrawing..." % remaining_deposit_amount)
            environment.send_contract_transaction(
                self.protocol.extra_deposit_contract, "withdraw", remaining_deposit_amount
            )

    def fund_state_channel(self, environment: Environment, funding_id: bytes) -> None:
        pass

    @property
    def protocol(self) -> StateChannelFileSaleExtraDeposit:
        return cast(StateChannelFileSaleExtraDeposit, self._protocol)
