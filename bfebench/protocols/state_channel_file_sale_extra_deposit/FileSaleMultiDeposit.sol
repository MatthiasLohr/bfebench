// This file is part of the Blockchain-based Fair Exchange Benchmark Tool
//    https://gitlab.com/MatthiasLohr/bfebench
//
// Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This contract is based on the FairSwap contract at https://github.com/lEthDev/FairSwap
// Original authors: Stefan Dziembowski, Lisa Eckey, Sebastian Faust

// SPDX-License-Identifier: Apache-2.0

pragma solidity ^0.7.0;
pragma experimental ABIEncoderV2;

import "../state_channel_file_sale/perun-eth-contracts/contracts/AssetHolder.sol";

contract FileSaleMultiDeposit {
    address payable public owner;
    AssetHolder public assetHolder;
    mapping(bytes32 => uint256) public holdings;

    /**
     * This contract is meant to be deployed by the seller as simple deposit service.
     */
    constructor(address payable depositOwner, AssetHolder perunAssetHolder) {
        owner = depositOwner;
        assetHolder = perunAssetHolder;
    }

    /**
     * Allow the seller (technically: anyone) to increase funds
     */
    function deposit(bytes32 channelID) public payable {
        holdings[channelID] += msg.value;
    }

    /**
     * Allow the seller to withdraw funds
     */
    function withdraw(uint amount, bytes32 channelID) public {
        require(msg.sender == owner);
        require(holdings[channelID] >= amount);
        holdings[channelID] -= amount;
        owner.transfer(amount);
    }

    /**
     * Allow the buyer to initiate state channel
     */
    function doStateChannelDeposits(bytes32 channelID, address participant) public payable {
        require(holdings[channelID] >= msg.value);
        uint sellerAmount = getSellerDepositAmount();
        require(msg.value >= sellerAmount);
        holdings[channelID] -= sellerAmount;
        // transfer funds from participant (buyer)
        assetHolder.deposit{value: msg.value}(calcFundingID(channelID, participant), msg.value);
        // transfer funds from seller
        assetHolder.deposit{value: msg.value}(calcFundingID(channelID, owner), sellerAmount);
    }

    /**
     * @notice Internal helper function that calculates the fundingID.
     * @param channelID ID of the channel.
     * @param participant Address of a participant in the channel.
     * @return The funding ID, an identifier used for indexing.
     */
    function calcFundingID(bytes32 channelID, address participant) internal pure returns (bytes32) {
        return keccak256(abi.encode(channelID, participant));
    }

    function getSellerDepositAmount() public returns (uint) {
        return 190000; // this can be chosen by the seller
    }
}
