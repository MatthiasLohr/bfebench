# This file is part of the Blockchain-based Fair Exchange Benchmark Tool
#    https://gitlab.com/MatthiasLohr/bfebench
#
# Copyright 2021-2023 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import logging
import os
from typing import Any

from eth_typing.evm import ChecksumAddress

from bfebench.contract import Contract, SolidityContractSourceCodeManager

from ...environment import Environment
from ..state_channel_file_sale.protocol import StateChannelFileSale

logger = logging.getLogger(__name__)


class StateChannelFileSaleExtraDeposit(StateChannelFileSale):
    EXTRA_DEPOSIT_CONTRACT_NAME: str
    EXTRA_DEPOSIT_CONTRACT_FILE: str

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._extra_deposit_contract: Contract | None = None

    def set_up_simulation(
        self,
        environment: Environment,
        seller_address: ChecksumAddress,
        buyer_address: ChecksumAddress,
    ) -> None:
        super().set_up_simulation(environment, seller_address, buyer_address)

        scscm = SolidityContractSourceCodeManager()
        scscm.add_contract_file(os.path.join(os.path.dirname(__file__), self.EXTRA_DEPOSIT_CONTRACT_FILE))
        contracts = scscm.compile(self.SOLC_VERSION)
        self._extra_deposit_contract = contracts[self.EXTRA_DEPOSIT_CONTRACT_NAME]

        if self._adjudicator_contract is None:
            raise RuntimeError("Adjudicator contract should be already initialized at this time")

        tx_receipt = environment.deploy_contract(
            self._extra_deposit_contract, seller_address, self.asset_holder_contract.address
        )
        logger.debug(
            "deployed extra deposit contract at %s (%s gas used)"
            % (self._extra_deposit_contract.address, tx_receipt["gasUsed"])
        )

    @property
    def extra_deposit_contract(self) -> Contract:
        if self._extra_deposit_contract is None:
            raise RuntimeError("accessing uninitialized contract")
        return self._extra_deposit_contract


class StateChannelFileSaleExtraMonoDeposit(StateChannelFileSaleExtraDeposit):
    EXTRA_DEPOSIT_CONTRACT_NAME = "FileSaleMonoDeposit"
    EXTRA_DEPOSIT_CONTRACT_FILE = "./FileSaleMonoDeposit.sol"


class StateChannelFileSaleExtraMultiDeposit(StateChannelFileSaleExtraDeposit):
    EXTRA_DEPOSIT_CONTRACT_NAME = "FileSaleMultiDeposit"
    EXTRA_DEPOSIT_CONTRACT_FILE = "./FileSaleMultiDeposit.sol"
